import redis


def main():
    r = redis.StrictRedis(host="redis", port=6379, decode_responses=True)
    p = r.pubsub()
    p.subscribe("node")

    for message in p.listen():
        print(message)
        print(type(message))


if __name__ == "__main__":
    main()
