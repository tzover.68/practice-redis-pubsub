const redis = require("redis");

const subscriber = redis.createClient(6379, "redis");

subscriber.subscribe("python");

subscriber.on("message", (message, channel) => {
    console.log(`Message: ${message} from ${channel}`);
});
