import redis
import time


def main():
    r = redis.StrictRedis(host="redis", port=6379, decode_responses=True)
    channel = "python"
    message = "hello from python"

    while (1):
        r.publish(channel, message)
        print("Redis published {} to {}".format(message, channel))
        time.sleep(3)


if __name__ == "__main__":
    main()
