const redis = require("redis");

const publisher = redis.createClient(6379, "redis");

const message = "hello";
const channel = "node";

setInterval(() => {
    publisher.publish(channel, message);
    console.log(`Redis published ${message} to ${channel}`);
}, 3000);
